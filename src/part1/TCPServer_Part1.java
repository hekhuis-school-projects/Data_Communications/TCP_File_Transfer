package part1;

import java.io.*;
import java.net.*;

public class TCPServer_Part1 {

	public static void main (String [] args ) throws IOException {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Boolean validPort = false;
		String portNumber = "";
		ServerSocket listenSocket = null;
		System.out.println("Please enter a port number to listen on: ");
		while(!validPort) {
			try {
				portNumber = inFromUser.readLine();
				listenSocket = new ServerSocket(Integer.parseInt(portNumber));
				System.out.println("Listening for connections on port " + portNumber + ".");
				validPort = true;
			} catch (NumberFormatException | IOException e1) {
				System.out.println("Not a valid port to use / Port unavailable.\nPlease enter a different port number: ");
			}
		}
		
		FileInputStream fis = null;
	    BufferedInputStream bis = null;
	    OutputStream os = null;
	    Socket clientSocket = null;
	    clientSocket = listenSocket.accept();
    	System.out.println("Connection made with a client.");
	    
    	while (true) {  
	    	
	    	BufferedReader inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	    	DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
	    	String filePath = inFromClient.readLine();
	    	File myFile = null;
	    	try {
	    		myFile = new File (filePath);
    		} catch (Exception e) {
    			outToClient.writeBytes("Not valid file" + '\n');
    			System.exit(0);
			}
	    	String fileLength = Integer.toString((int)myFile.length());
	    	System.out.println(fileLength);
	    	outToClient.writeBytes(fileLength + '\n');
	    	byte [] buffer  = new byte [(int)myFile.length()];
	    	fis = new FileInputStream(myFile);
	    	bis = new BufferedInputStream(fis);
	    	bis.read(buffer, 0, buffer.length);
	    	os = clientSocket.getOutputStream();
	    	System.out.println("Sending file: " + filePath);
	    	os.write(buffer, 0, buffer.length);
	    	
	    	//int count;
	        //while ((count = bis.read(buffer)) > 0) {
	        //    os.write(buffer, 0, count);
	        //    System.out.println(count);
	        //}
	    	
	    	System.out.println("File sending complete.");
	    	os.flush();
    	} 
    }
}
