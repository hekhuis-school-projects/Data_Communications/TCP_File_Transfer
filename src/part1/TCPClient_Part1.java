package part1;

import java.io.*;
import java.net.*;

public class TCPClient_Part1 {
	
	public static void main (String [] args ) throws IOException {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		InetAddress IPAddress = null;
		String portNumber = "";
		Socket clientSocket = null;
		try {
			System.out.println("Please enter an IP address to connect to: ");
			IPAddress = InetAddress.getByName(inFromUser.readLine());
			if(!IPAddress.isReachable(30)){
				System.out.println("Server unreachable.");
				System.exit(0);
			}
			System.out.println("Please enter a port number to connect to: ");
			portNumber = inFromUser.readLine();
			clientSocket = new Socket(IPAddress, Integer.parseInt(portNumber));
		} catch (NumberFormatException | IOException e) {
			System.out.println("Failed to connect to server. Not a valid IP or port number.");
			System.exit(0);
		}
		System.out.println("Connecting to the server at " + IPAddress.toString().substring(1) + " on port " + portNumber + ".");
		int bytesRead;
	    int current = 0;
	    FileOutputStream fos = null;
	    BufferedOutputStream bos = null;
	    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
	    System.out.println("Enter the path of the file you would like to download: ");
	    String filePath = inFromUser.readLine();
	    System.out.println("Enter the path of where to save the file: ");
	    String savePath = inFromUser.readLine();
	    outToServer.writeBytes(filePath + '\n');
	    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	    int fileSize = Integer.parseInt(inFromServer.readLine());
	    byte [] buffer  = new byte [fileSize];
	    InputStream is = clientSocket.getInputStream();
	    fos = new FileOutputStream(savePath);
	    bos = new BufferedOutputStream(fos);
	    bytesRead = is.read(buffer, 0, buffer.length);
	    current = bytesRead;
	    
	    while(bytesRead > 0) {
	    	bytesRead = is.read(buffer, current, (buffer.length - current));
	    	if(bytesRead >= 0)
	    		current += bytesRead;
	    }
	    
	    bos.write(buffer, 0 , current);
	    bos.flush();
	    System.out.println("File successfully downloaded. It can be found at: " + savePath);
	    fos.close();
	    bos.close();
	    clientSocket.close();
    }
}
