package part2;

import java.io.*;
import java.net.*;

/**
 * Creates a server using TCP sockets and a user specified port number. Allows for 
 * connection from multiple clients using separate threads for each client that
 * connects. Each client/server connection is capable of transferring multiple
 * file sequentially.
 * @author Kyle Hekhuis
 * @date 9/30/16
 */
public class TCPServer {

	public static void main (String [] args ) throws IOException {
		//Reader for user input
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Boolean validPort = false;
		String portNumber = "";
		ServerSocket listenSocket = null;
		System.out.println("Please enter a port number to listen on: ");
		//Getting a port number for the server from the user and checks if port is valid
		while(!validPort) {
			try {
				portNumber = inFromUser.readLine();
				listenSocket = new ServerSocket(Integer.parseInt(portNumber));
				System.out.println("Listening for connections on port " + portNumber + ".");
				validPort = true;
			} catch (NumberFormatException | IOException e1) {
				System.out.println("Not a valid port to use / Port unavailable.\nPlease enter a different port number: ");
			}
		}
		//Gets a client connection and starts a new thread for each client
	    Socket clientSocket = null;
	    ClientListener cl;
	    while(true) {  
	    	clientSocket = listenSocket.accept();
	    	System.out.println("Client connected.");
			cl = new ClientListener(clientSocket);
			cl.start();
    	}
    }
}

/**
 * Creates a thread that listens to a client socket for file name input and then sends
 * that file from the server to the client if the file exist on the server.
 * @author Kyle Hekhuis
 * @date 9/30/16
 */
class ClientListener extends Thread {
	
	Socket clientSocket;
	ClientListener(Socket connection) {
		clientSocket = connection;
	}
	
	public void run() {
		try{
			BufferedInputStream bis = null;
			//Reads information from the client
		    BufferedReader inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	    	//Sends information out to the client
		    DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
	    	String filePath = null;
	    	//Listens for a file name from the client and checks if it is a valid file
	    	//Returns 'VALID' to client if it's a valid file, or 'INVALID' if it's not
	    	while(true) {  
				boolean validFile = false;
				File myFile = null;
				while(!validFile) {
					filePath = inFromClient.readLine();
					try {
			    		myFile = new File (filePath);
			    		bis = new BufferedInputStream(new FileInputStream(myFile));
			    		outToClient.writeBytes("VALID" + '\n');
			    		String fileSize = Integer.toString((int)myFile.length());
			    		outToClient.writeBytes(fileSize + '\n');
			    		validFile = true;
					} catch (Exception e) {
		    			outToClient.writeBytes("INVALID" + '\n');
					}
		    	}   	
		    	//Buffer of bytes of file being sent
		    	byte [] buffer  = new byte [(int)myFile.length()];
		    	//Read file into buffer
		    	bis.read(buffer, 0, buffer.length);
		    	System.out.println("Sending file: " + filePath);
		    	//Send buffer to client
		    	outToClient.write(buffer, 0, buffer.length);
		    	System.out.println("File sending complete.");
	    	}
		} catch (Exception e) {
			System.out.println("Client disconnected.");
		}
	}
}
