package part2;

import java.io.*;
import java.net.*;

/**
 * Creates a client that connects to a server over a TCP socket using user
 * specified IP address and port number. Request and receives multiple files
 * from the server sequentially.
 * @author Kyle Hekhuis
 * @date 9/30/16
 */
public class TCPClient {
	
	public static void main (String [] args ) throws IOException {
		//Reader for user input
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		InetAddress IPAddress = null;
		String portNumber = "";
		Socket clientSocket = null;
		//Gets the IP address and port number of the server to connect to from the client user
		//Terminates program if IP and port are incorrect or client fails to connect
		//Can put in while loop if you want to allow client to retry
		try {
			System.out.println("Please enter an IP address to connect to: ");
			IPAddress = InetAddress.getByName(inFromUser.readLine());
			if(!IPAddress.isReachable(30)){
				System.out.println("Server unreachable.");
				System.exit(0);
			}
			System.out.println("Please enter a port number to connect to: ");
			portNumber = inFromUser.readLine();
			clientSocket = new Socket(IPAddress, Integer.parseInt(portNumber));
		} catch (NumberFormatException | IOException e) {
			System.out.println("Failed to connect to server. Not a valid IP or port number.");
			System.exit(0);
		}
		System.out.println("Connecting to the server at " + IPAddress.toString().substring(1) + " on port " + portNumber + ".");
		//Sends data out to server
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
	    //Reads data from the server
		BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	    FileOutputStream fos = null;
	    String filePath = "";
	    int fileSize = 0;
	    //Ask user for file path of the file they want from the server. Keeps trying until the file is valid.
	    //Lets user end session by typing /exit
	    while(true) {
	    	boolean validFile = false;
	    	while(!validFile) {
		    	try {
		    		System.out.println("Enter the path of the file you would like to download or type '/exit' to disconnect: ");
			    	filePath = inFromUser.readLine();
			    	//Terminate session if user types '/exit'
			    	if(filePath.equals("/exit")) {
				    	clientSocket.close();
			    		System.exit(0);
			    	}
			    	if(filePath.equals("")) {
			    		throw new Exception();
			    	}
			    	outToServer.writeBytes(filePath + '\n');
			    	//See if file is valid or invalid
			    	String fileValidity = inFromServer.readLine();
			    	//Get size of the file being sent
			    	if(fileValidity.equals("VALID")) {
			    		fileSize = Integer.parseInt(inFromServer.readLine());
			    		validFile = true;
			    	} else {
			    		throw new Exception();
			    	}
		    	} catch (Exception e) {
		    		System.out.println("File does not exist.");
		    	}
	    	}
	    	validFile = false;
	    	String savePath = null;
	    	//Ask user for file path of where to save the file they're receiving from server
	    	//Keeps trying until the file is valid
	    	while(!validFile) {
	    		try {
	    			System.out.println("Enter the path of where to save the file: ");
				    savePath = inFromUser.readLine();
				    if(savePath.equals("")) {
		    			throw new Exception();
		    		}
				    fos = new FileOutputStream(savePath);
				    validFile = true;
	    		} catch (Exception e) {
	    			System.out.println("Not a valid save path.");
	    		}
	    	}
	    	//Buffer to receive the file in
		    byte [] buffer  = new byte [fileSize];
		    InputStream is = clientSocket.getInputStream();
		    int count;
		    //Receives the file from the server. Knows when to stop based on how big the file is
		    //and how many bytes have been already read.
	        while (fileSize > 0 && (count = is.read(buffer, 0, (int)Math.min(buffer.length, fileSize))) != -1) {
	        	fos.write(buffer,0,count);
	        	fileSize -= count;
	        }
		    System.out.println("File successfully downloaded. It can be found at: " + savePath);
	    }
    }
}
