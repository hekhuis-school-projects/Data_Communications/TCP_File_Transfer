TCP File Transfer
Language: Java
Author: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 457 - Data Communications
Semester / Year: Fall 2016

This program creates a server / client setup where the client request
files from the server. The file transfer between the two is done over
TCP. For full specifications see 'CIS 457 Project 1_ TCP File Transfer.pdf'.

Usage:
Run 'TCPServer.java' and follow the input prompts to set up the server.
After the server is running, run 'TCPClient.java' and follow the input
prompts to connect to the server and then request a file.